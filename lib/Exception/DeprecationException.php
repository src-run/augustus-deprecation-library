<?php

/*
 * This file is part of the `src-run/augustus-deprecation-library` project.
 *
 * (c) Rob Frawley 2nd <rmf@src.run>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

namespace SR\Deprecation\Exception;

use SR\Exception\Exception;

/**
 * Class DeprecationException.
 */
class DeprecationException extends Exception
{
}

/* EOF */
